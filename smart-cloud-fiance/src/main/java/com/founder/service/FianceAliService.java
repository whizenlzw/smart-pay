package com.founder.service;

import com.founder.core.dao.FianceAlRespository;
import com.founder.core.dao.PayChannelRespository;
import com.founder.core.domain.FianceAl;
import com.founder.core.domain.PayChannel;
import com.founder.core.utils.FileUtil;
import com.founder.core.utils.FtpUtil;
import com.xxl.job.core.log.XxlJobLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class FianceAliService extends BaseService {

    @Autowired
    PayChannelRespository payChannelRespository;

    @Autowired
    FianceAlRespository fianceAlRespository;

    /**
     * 生成支付宝对账文件
     * @param mch
     * @param date
     * @param prefix
     * @param suffix
     * @param ftpOpen
     * @param ftpHome
     * @param subsys
     */
    public void generateAl(String mch,String date, String prefix, String suffix, String ftpOpen, String ftpHome, String subsys){

        XxlJobLogger.log("###### 开始商户" + mch + "生成支付宝对账单并上传FTP ######");

        String keyName = "%";
        XxlJobLogger.log("查询对账单");
        List<FianceAl> list = fianceAlRespository.queryAllByMchidAndBzorder(mch, date, keyName);
        XxlJobLogger.log("记录数：" + list.size());

        XxlJobLogger.log("生成对账文件");
        String content = super.buildContent(list, subsys, jobConfig.getHostipal_code());
        XxlJobLogger.log("对账文件内容。\r\n" + content);

        String fileName = prefix + "_" + suffix + "_" + date + ".txt";
        String filePath = jobConfig.getBill_path() + prefix + date + suffix + ".txt";
        XxlJobLogger.log("生成支付宝对账单." + filePath);
        FileUtil.saveFile(filePath, content);

        String ftpAddr = jobConfig.getFtp_iP();
        String ftpPort = jobConfig.getFtp_port();
        String ftpUser = jobConfig.getFtp_user();
        String ftpPass = jobConfig.getFtp_pass();
        if ("1".equals(ftpOpen)){
            FtpUtil ftp = new FtpUtil(ftpAddr,Integer.valueOf(ftpPort),ftpUser,ftpPass);
            Boolean flag = ftp.ftpUploadFile(ftpHome, fileName, filePath);
            if (flag) {
                XxlJobLogger.log("對賬文件上傳到ftp服務器成功！");
            } else {
                XxlJobLogger.log("對賬文件上傳到ftp服務器失敗！");
            }
        } else {
            XxlJobLogger.log("對賬文件未配置上傳到ftp服務器！");
        }
    }

    /**
     * 查找需要下载对账单的支付渠道
     * @param channelName
     * @return
     */
    public List<PayChannel> findPayChannel(String channelName){
        return payChannelRespository.findAllByChannelNameAndChannelId(channelName,"ALIPAY_QR");
    }

    /**
     * 判断对账单是否下载成功(需要优化)
     * @param mchId
     * @param date
     * @return 没下载返回true
     */
    public Boolean checkPayChannel(String mchId, String date){
        List<FianceAl> list = fianceAlRespository.queryAllByMchidAndBzorder(mchId, date, "%");
        return CollectionUtils.isEmpty(list);
    }
}
